import React, { useState, useEffect } from "react"
import { Link, useLocation, useNavigate } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import { makeColumns } from "../utils"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"

export default function TagList() {
    const [columns, setColumns] = useState([])
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const location = useLocation()
    const navigate = useNavigate()

    useEffect(() => {
        setLoading(true)
        setError(false)
        const parsed = queryString.parse(location.search)
        if (!parsed.page) parsed.page = 1
        authFetch("/api/tags?" + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                document.title = "Tags"
                setPage(data.page)
                setPageCount(Math.ceil(data.total / data.perPage))
                setColumns(makeColumns(data.columns, data.items))
                setLoading(false)
            })
            .catch(error => {
                console.log("error", error)
                setError(true)
                setLoading(false)
            })
    }, [location.search])

    if (loading) return <Loading />

    if (error) return <div>Erreur</div>

    return (
        <div>
            <br />
            <div className="level">
                <div className="level-item">
                    <nav className="pagination">
                        <ReactPaginate
                            initialPage={page - 1}
                            forcePage={page - 1}
                            onPageChange={p => {
                                navigate("/tags?page=" + (p.selected + 1))
                            }}
                            disableInitialCallback={true}
                            hrefBuilder={p => {
                                return "/tags?page=" + p
                            }}
                            pageCount={pageCount}
                            marginPagesDisplayed={2}
                            previousLabel="<"
                            nextLabel=">"
                            containerClassName="pagination-list"
                            pageLinkClassName="pagination-link"
                            previousLinkClassName="pagination-link"
                            nextLinkClassName="pagination-link"
                            activeLinkClassName="pagination-link is-current"
                            breakLinkClassName="pagination-ellipsis"
                        />
                    </nav>
                </div>
            </div>

            <div className="columns">
                {columns.map((col, idx) => {
                    return (
                        <div key={idx} className="column">
                            {col.map((item, idx) => {
                                return (
                                    <p key={idx}>
                                        <Link to={`/tag/${encodeURIComponent(item.rep)}`}>{item.rep}</Link>{" "}
                                        <small>{item.count}</small>
                                    </p>
                                )
                            })}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
