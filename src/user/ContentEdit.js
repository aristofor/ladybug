import { useState, useEffect } from "react"
import { useNavigate, useParams } from "react-router-dom"
import queryString from "query-string"
import EditForm from "../lib/EditForm"
import AssetForm from "../lib/AssetForm"
import { authFetch } from "../auth"

export default function ContentEdit() {
    const { id } = useParams()
    const [title, setTitle] = useState("")
    const [body, setBody] = useState("")
    const [tags, setTags] = useState([])
    const [assets, setAssets] = useState([])
    const navigate = useNavigate()

    const loadData = () => {
        authFetch(`/api/content/${id}?` + queryString.stringify({ raw: 1, assets: 1 }))
            .then(r => r.json())
            .then(data => {
                setTitle(data.title)
                setBody(data.body)
                setTags(data.tags)
                if (data.assets) setAssets(data.assets)
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    useEffect(() => {
        loadData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleSubmit = event => {
        event.preventDefault()
        const requestOptions = {
            method: "PUT",
            body: JSON.stringify({ title: title, body: body, tags: tags })
        }
        authFetch(`/api/content/${id}`, requestOptions)
            .then(r => r.json())
            .then(data => {
                if (!data.ok) console.log("Échec")
                else navigate(`/content/${data.id}`)
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    const handleUpload = event => {
        event.preventDefault()
        const input = document.getElementById("asset")
        if (!input.files[0]) return
        const formData = new FormData()
        formData.append("asset", input.files[0])
        const requestOptions = {
            method: "POST",
            headers: {}, // laisse le navigateur décider du Content-Type et du boundary
            body: formData
        }
        authFetch(`/api/content/${id}/upload`, requestOptions)
            .then(r => r.json())
            .then(data => {
                // TODO: traiter result.ok
                return authFetch(`/api/content/${id}/assets`).then(data => {
                    setAssets(data.assets)
                })
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    const handleAssetDelete = (event, name) => {
        event.preventDefault()
        authFetch(`/api/content/${id}/asset/${name}`, { method: "DELETE" })
            .then(r => r.json())
            .then(data => {
                if (data.ok) {
                    const newAssets = assets.filter(value => value !== name)
                    setAssets(newAssets)
                }
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    return (
        <div>
            <br />
            <div className="columns">
                <div className="column">
                    <EditForm
                        onSubmit={handleSubmit}
                        onReset={loadData}
                        title={title}
                        setTitle={setTitle}
                        body={body}
                        setBody={setBody}
                        tags={tags}
                        setTags={setTags}>
                        <div className="field is-grouped">
                            <div className="control">
                                <button type="submit" className="button is-primary">
                                    Submit
                                </button>
                            </div>
                            <div className="control">
                                <button type="reset" className="button is-light">
                                    Reset
                                </button>
                            </div>
                        </div>
                    </EditForm>
                </div>
                <div className="column is-one-fifth">
                    <label className="label">Assets</label>
                    <AssetForm id={id} assets={assets} onSubmit={handleUpload} onAssetDelete={handleAssetDelete} />
                </div>
            </div>
        </div>
    )
}
