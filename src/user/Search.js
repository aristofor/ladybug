import React, { useState, useEffect } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import ContentListItem from "../lib/ContentListItem"
import { makeColumns } from "../utils"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"

export default function Search() {
    const [columns, setColumns] = useState([])
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const location = useLocation()
    const navigate = useNavigate()

    useEffect(() => {
        document.title = "Search"
        const parsed = queryString.parse(location.search)
        if (!parsed.q) return
        if (!parsed.page) parsed.page = 1
        authFetch("/api/search?" + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                setPage(data.page)
                setPageCount(Math.ceil(data.total / data.perPage))
                setColumns(makeColumns(data.columns, data.items))
                setLoading(false)
            })
            .catch(error => {
                console.log("error", error)
                setError(true)
                setLoading(false)
            })
    }, [location.search])

    if (loading) return <Loading />

    if (error) return <div>Erreur</div>

    return (
        <div>
            <br />
            <div className="level">
                <div className="level-item">
                    <nav className="pagination">
                        <ReactPaginate
                            initialPage={page - 1}
                            onPageChange={p => {
                                navigate(
                                    "/search?q=" + queryString.parse(location.search).q + "&page=" + (p.selected + 1)
                                )
                            }}
                            disableInitialCallback={true}
                            hrefBuilder={p => {
                                return "/search?q=" + queryString.parse(location.search).q + "&page=" + p
                            }}
                            pageCount={pageCount}
                            marginPagesDisplayed={2}
                            previousLabel="<"
                            nextLabel=">"
                            containerClassName="pagination-list"
                            pageLinkClassName="pagination-link"
                            previousLinkClassName="pagination-link"
                            nextLinkClassName="pagination-link"
                            activeLinkClassName="pagination-link is-current"
                            breakLinkClassName="pagination-ellipsis"
                        />
                    </nav>
                </div>
            </div>

            <div className="columns">
                {columns.map((col, idx) => {
                    return (
                        <div key={idx} className="column">
                            {col.map((item, idx) => {
                                return <ContentListItem key={idx} id={item.id} title={item.title} tags={item.tags} />
                            })}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
