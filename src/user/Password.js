import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import { authFetch } from "../auth"

export default function Password() {
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [error, setError] = useState(false)
    const [message, setMessage] = useState("")
    const navigate = useNavigate()

    const handleSubmit = event => {
        event.preventDefault()
        if (password1 !== password2) {
            setMessage("Passwords don't match.")
            setError(true)
            return
        }
        const requestOptions = {
            method: "POST",
            body: JSON.stringify({ password: password1 })
        }
        authFetch("/api/password", requestOptions)
            .then(r => r.json())
            .then(data => {
                if (data.ok) navigate("/contents")
                else {
                    setMessage(data.msg)
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    return (
        <section className="hero is-halfheight">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column is-offset-4 is-4">
                            <h1 className="title">Set password</h1>
                            <form onSubmit={handleSubmit}>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">
                                            New password
                                            <input
                                                autoFocus
                                                type="password"
                                                className="input"
                                                defaultValue={password1}
                                                onChange={e => {
                                                    setPassword1(e.target.value)
                                                }}
                                            />
                                        </label>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">
                                            Confirm
                                            <input
                                                type="password"
                                                className="input"
                                                defaultValue={password2}
                                                onChange={e => {
                                                    setPassword2(e.target.value)
                                                }}
                                            />
                                        </label>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <input type="submit" value="Submit" className="button is-primary" />
                                    </div>
                                </div>
                            </form>
                            {error ? (
                                <>
                                    <br />
                                    <div className="notification is-danger">{message}</div>
                                </>
                            ) : (
                                ""
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
