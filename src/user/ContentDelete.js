import React, { useState, useEffect } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { authFetch } from "../auth"

export default function ContentDelete() {
    const { id } = useParams()
    const [confirm, setConfirm] = useState(false)
    const navigate = useNavigate()

    useEffect(() => {
        if (!confirm) return
        authFetch(`/api/content/${id}`, { method: "DELETE" })
            .then(r => r.json())
            .then(data => {})
            .catch(error => {
                console.log("error", error)
            })
    }, [confirm, id])

    if (confirm)
        return (
            <div>
                <br />
                <code>{id}</code> deleted.
            </div>
        )
    else
        return (
            <form
                onSubmit={e => {
                    e.preventDefault()
                    setConfirm(true)
                }}
                onReset={() => {
                    navigate(-1)
                }}>
                Vraiment?
                <div className="control">
                    <button type="submit" className="button is-danger">
                        Delete
                    </button>
                    &nbsp;
                    <button type="reset" className="button is-link">
                        Oh no!
                    </button>
                </div>
            </form>
        )
}
