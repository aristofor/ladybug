import React, { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import EditForm from "../lib/EditForm"
import AssetForm from "../lib/AssetForm"
import { authFetch } from "../auth"

export default function ContentCreate() {
    const [title, setTitle] = useState("")
    const [body, setBody] = useState("")
    const [tags, setTags] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        document.title = "Write"
    })

    const handleSubmit = event => {
        event.preventDefault()
        const requestOptions = {
            method: "POST",
            body: JSON.stringify({ title: title, body: body, tags: tags })
        }
        authFetch("/api/content", requestOptions)
            .then(r => r.json())
            .then(data => {
                if (!data.ok) console.log("Échec")
                if (data.id) navigate("/content/" + data.id)
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    const handleUpload = event => {
        event.preventDefault()
        const input = document.getElementById("asset")
        if (!input.files[0]) return
        const formData = new FormData()
        formData.append("asset", input.files[0])
        const requestOptions = {
            method: "POST",
            headers: {}, // laisse le navigateur décider du Content-Type et du boundary
            body: formData
        }
        authFetch("/api/content/upload", requestOptions)
            .then(r => r.json())
            .then(data => {
                if (data.ok) {
                    const requestOptions = {
                        method: "PUT",
                        body: JSON.stringify({ title: title, body: body, tags: tags })
                    }
                    authFetch("/api/content/" + data.id, requestOptions)
                        .then(r => r.json())
                        .then(data => {
                            if (!data.ok) console.log("Échec")
                        })
                        .catch(error => {
                            console.log("error", error)
                        })
                        .finally(() => {
                            navigate("/content/" + data.id + "/edit")
                        })
                } else console.log("Échec")
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    return (
        <div>
            <br />
            <div className="columns">
                <div className="column">
                    <EditForm
                        onSubmit={handleSubmit}
                        title={title}
                        setTitle={setTitle}
                        body={body}
                        setBody={setBody}
                        tags={tags}
                        setTags={setTags}>
                        <div className="field is-grouped">
                            <div className="control">
                                <button type="submit" className="button is-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </EditForm>
                </div>
                <div className="column is-one-fifth">
                    <label className="label">Assets</label>
                    <AssetForm onSubmit={handleUpload} />
                </div>
            </div>
        </div>
    )
}
