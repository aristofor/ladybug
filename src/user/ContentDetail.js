import React, { useState, useEffect } from "react"
import { Link, useParams } from "react-router-dom"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"

export default function ContenDetail() {
    const [loading, setLoading] = useState(true)
    const { id } = useParams()
    const [error, setError] = useState(false)
    const [title, setTitle] = useState("")
    const [html, setHtml] = useState("")
    const [tags, setTags] = useState([])
    const [mtime, setMtime] = useState(null)

    useEffect(() => {
        setLoading(true)
        setError(false)
        authFetch(`/api/content/${id}`)
            .then(r => r.json())
            .then(data => {
                document.title = data.title || "(untitled)"
                setTitle(data.title)
                setHtml(data.html)
                setTags(data.tags)
                setMtime(data.mtime)
            })
            .catch(error => {
                console.log("error", error)
                setError(true)
            })
            .finally(() => {
                setLoading(false)
            })
    }, [id])

    if (loading) return <Loading />

    if (error) return <div>Erreur</div>

    return (
        <div className="content">
            <br />
            <div className="level">
                <h1 className="title">{title}</h1>
                <div className="level-right">
                    <Link to={`/content/${id}/edit`} className="button is-link is-small">
                        <span className="icon is-small">
                            <i className="fas fa-edit"></i>
                        </span>
                        <span>Edit</span>
                    </Link>
                    &nbsp;
                    <Link to={`/content/${id}/delete`} className="button is-danger is-small">
                        <span className="icon is-small">
                            <i className="fas fa-trash-alt"></i>
                        </span>
                        <span>Delete</span>
                    </Link>
                </div>
            </div>

            <div className="level">
                <div className="level-left tags">
                    {tags.map((rep, idx) => {
                        return (
                            <React.Fragment key={idx}>
                                <Link to={`/tag/${encodeURIComponent(rep)}`} className="tag">
                                    {rep}
                                </Link>
                            </React.Fragment>
                        )
                    })}
                </div>
                <div className="level-right">
                    <p className="is-size-7">{mtime}</p>
                </div>
            </div>
            <div dangerouslySetInnerHTML={{ __html: html }}></div>
        </div>
    )
}
