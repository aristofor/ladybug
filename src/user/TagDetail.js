import React, { useState, useEffect } from "react"
import { useLocation, useNavigate, useParams } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import ContentListItem from "../lib/ContentListItem"
import { makeColumns } from "../utils"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"

export default function TagDetail() {
    const [columns, setColumns] = useState([])
    const { rep } = useParams()
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const location = useLocation()
    const navigate = useNavigate()

    useEffect(() => {
        const parsed = queryString.parse(location.search)
        if (!parsed.page) parsed.page = 1
        authFetch("/api/tag/" + rep + "?" + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                document.title = "#" + rep
                setPage(data.page)
                setPageCount(Math.ceil(data.total / data.perPage))
                setColumns(makeColumns(data.columns, data.items))
                setLoading(false)
            })
            .catch(error => {
                console.log("error", error)
                setError(true)
            })
    }, [location.search, rep])

    if (loading) return <Loading />

    if (error) return <div className="notification is-danger">Erreur</div>

    return (
        <div>
            <div className="tag is-large">{decodeURIComponent(rep)}</div>
            <br />
            <div className="level">
                <div className="level-item">
                    <nav className="pagination">
                        <ReactPaginate
                            initialPage={page - 1}
                            forcePage={page - 1}
                            onPageChange={p => {
                                navigate("/tag/" + rep + "?page=" + (p.selected + 1))
                            }}
                            disableInitialCallback={true}
                            hrefBuilder={p => {
                                return "/tag/" + rep + "?page=" + p
                            }}
                            pageCount={pageCount}
                            marginPagesDisplayed={2}
                            previousLabel="<"
                            nextLabel=">"
                            containerClassName="pagination-list"
                            pageLinkClassName="pagination-link"
                            previousLinkClassName="pagination-link"
                            nextLinkClassName="pagination-link"
                            activeLinkClassName="pagination-link is-current"
                            breakLinkClassName="pagination-ellipsis"
                        />
                    </nav>
                </div>
            </div>

            <div className="columns">
                {columns.map((col, idx) => {
                    return (
                        <div key={idx} className="column">
                            {col.map((item, idx) => {
                                return <ContentListItem key={idx} id={item.id} title={item.title} tags={item.tags} />
                            })}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
