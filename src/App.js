import React, { useContext, useLayoutEffect, useState } from "react"
import { Link, Router, Navigate, Outlet, Route, Routes, useLocation } from "react-router-dom"
import { createBrowserHistory } from "history"
import { AuthContext, AuthContextProvider, useAuth } from "./auth"
import Navigation from "./lib/Navigation"
import "./App.css"

import IconLadyBug from "./IconLadyBug"
import StaticContent from "./main/StaticContent"
import Login from "./auth/Login"

import loadable from "@loadable/component"

const ForgotPassword = loadable(() => import(/* webpackChunkName: "recover" */ "./main/ForgotPassword"))
const ForgotPasswordToken = loadable(() => import(/* webpackChunkName: "recover" */ "./main/ForgotPasswordToken"))
const Logout = loadable(() => import(/* webpackChunkName: "user" */ "./auth/Logout"))
const ContentList = loadable(() => import(/* webpackChunkName: "user" */ "./user/ContentList"))
const ContentDetail = loadable(() => import(/* webpackChunkName: "user" */ "./user/ContentDetail"))
const ContentEdit = loadable(() => import(/* webpackChunkName: "user" */ "./user/ContentEdit"))
const ContentCreate = loadable(() => import(/* webpackChunkName: "user" */ "./user/ContentCreate"))
const ContentDelete = loadable(() => import(/* webpackChunkName: "user" */ "./user/ContentDelete"))
const TagList = loadable(() => import(/* webpackChunkName: "user" */ "./user/TagList"))
const TagDetail = loadable(() => import(/* webpackChunkName: "user" */ "./user/TagDetail"))
const Search = loadable(() => import(/* webpackChunkName: "user" */ "./user/Search"))
const Password = loadable(() => import(/* webpackChunkName: "user" */ "./user/Password"))

const Dashboard = loadable(() => import(/* webpackChunkName: "admin" */ "./admin/Dashboard"))
const Trash = loadable(() => import(/* webpackChunkName: "admin" */ "./admin/Trash"))

const PrivateOutlet = () => {
    const location = useLocation()
    const userInfo = useAuth()
    return userInfo ? <Outlet /> : <Navigate to={"/login"} state={{ next: location.pathname }} />
}

const AdminOutlet = () => {
    const location = useLocation()
    const authContext = useContext(AuthContext)
    const userInfo = authContext?.userInfo
    return userInfo ? (
        authContext.isAdmin ? (
            <Outlet />
        ) : (
            <h2 className="title">Accès interdit.</h2>
        )
    ) : (
        <Navigate to={"/login"} state={{ next: location.pathname }} />
    )
}

const history = createBrowserHistory()
history.listen((location, action) => {
    // replie le menu
    const el = document.getElementById("nav-toggle-state")
    if (el) el.checked = false
})

const CustomRouter = ({ basename, children, history }) => {
    const [state, setState] = useState({
        action: history.action,
        location: history.location
    })

    useLayoutEffect(() => history.listen(setState), [history])

    return (
        <Router
            basename={basename}
            children={children}
            location={state.location}
            navigationType={state.action}
            navigator={history}
        />
    )
}

export default function App() {
    return (
        <CustomRouter history={history}>
            <AuthContextProvider>
                <section className="section">
                    <Navigation />

                    <div className="container main-content">
                        <Routes>
                            <Route exact path="/" element={<StaticContent slug="home" />} />
                            <Route exact path="/login" element={<Login />} />
                            <Route exact path="/about" element={<StaticContent slug="about" />} />
                            <Route exact path="/recover" element={<ForgotPassword />} />
                            <Route exact path="/recover/:email" element={<ForgotPasswordToken />} />
                            <Route path="/" element={<PrivateOutlet />}>
                                {/* user */}
                                <Route exact path="/logout" element={<Logout />} />
                                <Route exact path="/contents" element={<ContentList />} />
                                <Route exact path="/content/create" element={<ContentCreate />} />
                                <Route exact path="/content/:id/edit" element={<ContentEdit />} />
                                <Route exact path="/content/:id/delete" element={<ContentDelete />} />
                                <Route exact path="/content/:id" element={<ContentDetail />} />
                                <Route exact path="/tags" element={<TagList />} />
                                <Route exact path="/tag/:rep" element={<TagDetail />} />
                                <Route exact path="/search" element={<Search />} />
                                <Route exact path="/password" element={<Password />} />
                            </Route>
                            {/* admin */}
                            <Route path="/" element={<AdminOutlet />}>
                                <Route exact path="/dashboard" element={<Dashboard />} />
                                <Route exact path="/trash" element={<Trash />} />
                            </Route>
                            {/* catchall */}
                            <Route path="*" element={<h2 className="title">Not found</h2>} />
                        </Routes>
                    </div>

                    <footer className="footer">
                        <div className="level">
                            <div className="level-item">
                                <Link className="navbar-item" to="/about">
                                    <span className="icon">
                                        <IconLadyBug />
                                    </span>
                                    &nbsp;
                                    <span>About</span>
                                </Link>
                            </div>
                        </div>
                    </footer>
                </section>
            </AuthContextProvider>
        </CustomRouter>
    )
}
