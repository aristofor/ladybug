import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"

export default function Dashboard() {
    const [siteName, setSiteName] = useState("")
    const [description, setDescription] = useState("")
    const [theme, setTheme] = useState("")
    const [themes, setThemes] = useState([])
    const [highlight, setHighlight] = useState("")
    const [highlights, setHighlights] = useState([])
    const [pageRows, setPageRows] = useState(null)
    const [pageCols, setPageCols] = useState(null)
    const [tagsRows, setTagsRows] = useState(null)
    const [tagsCols, setTagsCols] = useState(null)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const navigate = useNavigate()

    const handleClearCache = () => {
        authFetch("/api/admin/clear_cache")
            .then(r => r.json())
            .then(data => {
                console.log(data.msg)
            })
    }

    useEffect(() => {
        setLoading(true)
        setError(false)
        document.title = "LadyBug Dashboard"
        loadLayout()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const loadLayout = () => {
        authFetch("/api/admin/layout")
            .then(r => r.json())
            .then(data => {
                const layout = data.layout
                setSiteName(layout.sitename)
                setDescription(layout.description)
                setTheme(layout.theme)
                setThemes(data.themes)
                setHighlight(layout.highlight)
                setHighlights(data.highlights)
                setPageRows(layout.page_rows)
                setPageCols(layout.page_cols)
                setTagsRows(layout.tags_rows)
                setTagsCols(layout.tags_cols)
            })
            .catch(error => {
                console.log("error", error)
                setError(true)
            })
            .finally(() => {
                setLoading(false)
            })
    }

    const handleLayoutSubmit = event => {
        event.preventDefault()
        const requestOptions = {
            method: "POST",
            body: JSON.stringify({
                sitename: siteName,
                description: description,
                theme: theme,
                highlight: highlight,
                page_rows: parseInt(pageRows),
                page_cols: parseInt(pageCols),
                tags_rows: parseInt(tagsRows),
                tags_cols: parseInt(tagsCols)
            })
        }
        authFetch("/api/admin/layout", requestOptions)
            .then(result => {
                if (result.ok) navigate(0)
                else console.log("Échec")
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    if (loading) return <Loading />

    if (error) return <div>Erreur</div>

    return (
        <div className="content">
            <br />
            <h2 className="subtitle">Layout</h2>
            <form onSubmit={handleLayoutSubmit} onReset={loadLayout}>
                <div className="columns">
                    <div className="column">
                        <div className="field">
                            <label className="label">
                                Site name
                                <input
                                    className="input"
                                    type="text"
                                    spellCheck="false"
                                    defaultValue={siteName}
                                    onChange={e => {
                                        setSiteName(e.target.value)
                                    }}
                                />
                            </label>
                        </div>
                        <div className="field">
                            <label className="label">
                                Description
                                <textarea
                                    className="textarea"
                                    rows="3"
                                    spellCheck="false"
                                    defaultValue={description}
                                    onChange={e => {
                                        setDescription(e.target.value)
                                    }}></textarea>
                            </label>
                        </div>
                    </div>
                    <div className="column">
                        <div className="field">
                            <label className="label">
                                Theme
                                <br />
                                <div className="select">
                                    <select
                                        value={theme}
                                        onChange={e => {
                                            setTheme(e.target.value)
                                        }}>
                                        {themes.map((value, idx) => {
                                            return (
                                                <option key={idx} value={value}>
                                                    {value}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </label>
                        </div>
                        <div className="field">
                            <label className="label">
                                Highlight
                                <br />
                                <div className="select">
                                    <select
                                        value={highlight}
                                        onChange={e => {
                                            setHighlight(e.target.value)
                                        }}>
                                        {highlights.map((value, idx) => {
                                            return (
                                                <option key={idx} value={value}>
                                                    {value}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div className="column">
                        Sizes
                        <table className="table">
                            <tbody>
                                <tr>
                                    <td style={{ verticalAlign: "middle" }}>
                                        <label className="label">Page rows</label>
                                    </td>
                                    <td>
                                        <input
                                            defaultValue={pageRows}
                                            onChange={e => {
                                                setPageRows(e.target.value)
                                            }}
                                            className="input"
                                            type="number"
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ verticalAlign: "middle" }}>
                                        <label className="label">Page columns</label>
                                    </td>
                                    <td>
                                        <input
                                            defaultValue={pageCols}
                                            onChange={e => {
                                                setPageCols(e.target.value)
                                            }}
                                            className="input"
                                            type="number"
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ verticalAlign: "middle" }}>
                                        <label className="label">Tags rows</label>
                                    </td>
                                    <td>
                                        <input
                                            defaultValue={tagsRows}
                                            onChange={e => {
                                                setTagsRows(e.target.value)
                                            }}
                                            className="input"
                                            type="number"
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ verticalAlign: "middle" }}>
                                        <label className="label">Tags columns</label>
                                    </td>
                                    <td>
                                        <input
                                            defaultValue={tagsCols}
                                            onChange={e => {
                                                setTagsCols(e.target.value)
                                            }}
                                            className="input"
                                            type="number"
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="field is-grouped">
                    <div className="control">
                        <button type="submit" className="button is-primary">
                            Submit
                        </button>
                    </div>
                    <div className="control">
                        <button type="reset" className="button is-light">
                            Reset
                        </button>
                    </div>
                </div>
            </form>
            <hr />
            <button
                className="button"
                onClick={e => {
                    handleClearCache()
                }}>
                Clear cache
            </button>
        </div>
    )
}
