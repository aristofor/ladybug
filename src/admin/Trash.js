import React, { useEffect, useState } from "react"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"
import CloseOnEscape from "../lib/CloseOnEscape"

const Modal = ({ closeModal, modalState, id, handleRestore }) => {
    const [text, setText] = useState("")

    useEffect(() => {
        if (!id) return
        authFetch(`/api/admin/trash/item/${id}`)
            .then(r => r.json())
            .then(data => {
                setText(data.text)
            })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id])

    if (!modalState) return null

    return (
        <div className="modal is-active">
            <div className="modal-background" onClick={closeModal}></div>
            <div className="modal-card" style={{ width: "99%" }}>
                <header className="modal-card-head">
                    <code className="modal-card-title">{id}</code>
                    <button className="delete" aria-label="close" onClick={closeModal}></button>
                </header>
                <section className="modal-card-body">
                    <pre>{text}</pre>
                </section>
                <footer className="modal-card-foot">
                    <button className="button is-primary" onClick={handleRestore}>
                        Restore
                    </button>
                    <button className="button" onClick={closeModal}>
                        Close
                    </button>
                </footer>
            </div>
        </div>
    )
}

export default function Trash() {
    const [entries, setEntries] = useState([])
    const [modalState, setModalState] = useState(false)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const [id, setId] = useState(null)

    const handleClose = event => {
        if (event) event.preventDefault()
        setModalState(false)
    }

    const handleRestore = event => {
        event.preventDefault()
        authFetch(`/api/admin/trash/item/${id}/restore`)
            .then(r => r.json())
            .then(data => {
                setModalState(false)
                if (data.ok) setEntries(entries.filter((item, idx) => item.id !== id))
            })
    }

    useEffect(() => {
        document.title = "LadyBug Trash"
        setLoading(true)
        setError(false)
        authFetch("/api/admin/trash")
            .then(r => r.json())
            .then(data => {
                setEntries(data.trash)
            })
            .catch(error => {
                console.log("error", error)
                setError(true)
            })
            .finally(() => {
                setLoading(false)
            })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    if (loading) return <Loading />

    if (error) return <div>Erreur</div>

    return (
        <div className="content">
            <br />
            <h2 className="subtitle">Trash</h2>
            <table className="table is-hoverable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Assets</th>
                        <th>Deletion date</th>
                    </tr>
                </thead>
                <tbody>
                    {entries.map((item, idx) => {
                        return (
                            <tr
                                key={idx}
                                onClick={e => {
                                    setId(item.id)
                                    setModalState(true)
                                }}>
                                <td>
                                    <code>{item.id}</code>
                                </td>
                                <td>
                                    {item.assets && (
                                        <span className="icon is-small">
                                            <i className="fas fa-paperclip"></i>
                                        </span>
                                    )}
                                </td>
                                <td>
                                    <code>{item.mtime}</code>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <CloseOnEscape onEscape={handleClose}>
                <Modal modalState={modalState} closeModal={handleClose} id={id} handleRestore={handleRestore} />
            </CloseOnEscape>
        </div>
    )
}
