const { createProxyMiddleware } = require("http-proxy-middleware")
module.exports = function (app) {
    app.use(
        ["/api", "/asset", "/debug_static"],
        createProxyMiddleware({
            target: "http://localhost:5000",
            changeOrigin: true
        })
    )
}
