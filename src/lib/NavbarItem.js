import React from "react"
import { Link } from "react-router-dom"
import { pure } from "recompose"

export default pure(props => {
    return (
        <Link to={props.to} className="navbar-item">
            {props.icon && (
                <>
                    <span className="icon is-small">
                        <i className={props.icon}></i>
                    </span>
                    &nbsp;&nbsp;
                </>
            )}
            <span>{props.label}</span>
        </Link>
    )
})
