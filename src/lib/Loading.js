import React from "react"
import { pure } from "recompose"

export default pure(() => {
    return (
        <section className="hero is-halfheight">
            <div className="hero-body">
                <div className="container">
                    <div className="level">
                        <div className="level-item has-text-centered">
                            <div className="loader"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
})
