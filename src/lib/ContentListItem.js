import React from "react"
import { Link } from "react-router-dom"
import { pure } from "recompose"

export default pure(props => {
    let id = props.id
    let title = props.title
    let tags = props.tags || []
    return (
        <article className="media">
            <div className="media-content">
                <p>
                    <Link to={`/content/${id}`}>{title ? title : <em>(untitled)</em>}</Link>
                </p>
                <p className="tags">
                    {tags.map((rep, idx) => {
                        return (
                            <React.Fragment key={idx}>
                                <Link to={`/tag/${encodeURIComponent(rep)}`} className="tag">
                                    {rep}
                                </Link>
                            </React.Fragment>
                        )
                    })}
                </p>
            </div>
        </article>
    )
})
