// source : https://github.com/conorhastings/react-close-on-escape

import React from "react"
import PropTypes from "prop-types"

export default class CloseOnEscape extends React.Component {
    constructor() {
        super()
        this.onEscape = this.onEscape.bind(this)
    }

    onEscape({ keyCode }) {
        if (keyCode === 27) {
            this.props.onEscape()
        }
    }

    componentDidMount() {
        document.addEventListener("keydown", this.onEscape)
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onEscape)
    }

    render() {
        return this.props.children
    }
}

CloseOnEscape.propTypes = {
    onEscape: PropTypes.func.isRequired
}
