import React, { useRef, useEffect, useState } from "react"
import PropTypes from "prop-types"

export default function TagsInput(props) {
    const [tag, setTag] = useState("")
    const tagInput = useRef(null)

    useEffect(() => {
        if (props.autoFocus) tagInput.current.focus()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const removeTag = index => {
        const newTags = props.tags.filter((rep, idx) => idx !== index)
        props.onChange(newTags)
    }

    const handleKeyDown = event => {
        let keyCode = event.keyCode
        if (keyCode === 13) {
            event.preventDefault()
            const rep = tag.trim()
            if (rep === "") {
                tagInput.current.value = ""
                return
            }
            if (props.tags.indexOf(rep) === -1) {
                const newTags = [...props.tags, rep]
                props.onChange(newTags)
            }
            setTag("")
            tagInput.current.value = ""
        }
    }

    return (
        <div
            className="field is-grouped is-grouped-multiline tags"
            onFocus={e => {
                tagInput.current.focus()
            }}
            onClick={e => {
                e.preventDefault()
            }}>
            {props.tags.map((rep, idx) => {
                return (
                    <React.Fragment key={idx}>
                        <span className="tag">
                            {rep}
                            <span
                                className="delete is-small"
                                onClick={e => {
                                    e.preventDefault()
                                    removeTag(idx)
                                }}></span>
                        </span>
                    </React.Fragment>
                )
            })}
            &nbsp;
            <span>
                <input
                    ref={tagInput}
                    className="input is-small"
                    defaultValue={tag}
                    spellCheck="false"
                    onChange={e => {
                        e.preventDefault()
                        setTag(e.target.value)
                    }}
                    onKeyDown={handleKeyDown}
                    placeholder="Add tag..."
                    {...props.inputProps}
                />
            </span>
        </div>
    )
}

TagsInput.propTypes = {
    tags: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    inputProps: PropTypes.object
}
