import { useContext } from "react"
import { Link } from "react-router-dom"
import NavbarItem from "./NavbarItem"
import SearchBox from "./SearchBox"
import { AuthContext } from "../auth"

export default function Navigation() {
    const authContext = useContext(AuthContext)
    const userInfo = authContext?.userInfo
    return (
        <nav className="navbar">
            <div className="container">
                <div className="navbar-brand">
                    <NavbarItem to="/" label="Home" icon="fas fa-home" />
                    <label
                        role="button"
                        className="navbar-burger burger"
                        aria-label="menu"
                        aria-expanded="false"
                        htmlFor="nav-toggle-state">
                        <span aria-hidden="true" />
                        <span aria-hidden="true" />
                        <span aria-hidden="true" />
                    </label>
                </div>
                <input type="checkbox" id="nav-toggle-state" />
                <div className="navbar-menu">
                    {userInfo ? (
                        <>
                            <div className="navbar-start">
                                <NavbarItem to="/contents" label="Contents" icon="fas fa-book" />
                                <NavbarItem to="/tags" label="Tags" icon="fas fa-tags" />
                                <NavbarItem to="/content/create" label="Write" icon="fas fa-edit" />
                                <div className="navbar-item">
                                    <SearchBox />
                                </div>
                            </div>
                            <div className="navbar-end">
                                <div className="navbar-item has-dropdown is-hoverable">
                                    <div className="navbar-link">
                                        <em>{userInfo.username}</em>
                                        <div className="navbar-dropdown">
                                            <Link to="/password" className="button is-white">
                                                <span className="icon is-small">
                                                    <i className="fas fa-key"></i>
                                                </span>
                                                <span>Password</span>
                                            </Link>
                                            {authContext.isAdmin && (
                                                <>
                                                    <Link to="/dashboard" className="button is-white">
                                                        <span className="icon is-small">
                                                            <i className="fas fa-cog"></i>
                                                        </span>
                                                        <span>Dashboard</span>
                                                    </Link>
                                                    <Link to="/trash" className="button is-white">
                                                        <span className="icon is-small">
                                                            <i className="fas fa-trash-alt"></i>
                                                        </span>
                                                        <span>Trash</span>
                                                    </Link>
                                                </>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <NavbarItem to="/logout" label="Logout" icon="fas fa-power-off" />
                            </div>
                        </>
                    ) : (
                        <div className="navbar-end">
                            <NavbarItem to="/login" label="Login" icon="fas fa-user" />
                        </div>
                    )}
                </div>
            </div>
        </nav>
    )
}
