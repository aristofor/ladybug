import React, { useState } from "react"
import CloseOnEscape from "./CloseOnEscape"

const Modal = ({ closeModal, modalState, id, asset }) => {
    if (!modalState) return null

    const l = id.length
    const shard = id.charAt(l - 1) + "/" + id.charAt(l - 2) + "/" + id
    return (
        <div className="modal is-active">
            <div className="modal-background" onClick={closeModal}></div>
            <div className="modal-content has-text-centered">
                <p className="image">
                    <img src={`/asset/${shard}/${asset}`} alt="" />
                </p>
            </div>
            <button className="modal-close is-large" aria-label="close" onClick={closeModal}></button>
        </div>
    )
}

export default function AssetForm({ id, assets, onSubmit, onAssetDelete }) {
    const [modalState, setModalState] = useState(false)
    const [asset, setAsset] = useState("")

    const handleClose = event => {
        setModalState(false)
        if (event) event.preventDefault()
    }

    const handleAssetClick = (event, name) => {
        event.preventDefault()
        setAsset(name)
        setModalState(true)
    }

    const handleAssetCopy = (event, name) => {
        event.preventDefault()
        const el = document.createElement("textarea")
        el.value = "![](" + name + ")"
        el.setAttribute("readonly", "")
        el.style.position = "absolute"
        el.style.left = "-9999px"
        document.body.appendChild(el)
        const selected = document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false
        el.select()
        const result = document.execCommand("copy")
        document.body.removeChild(el)
        if (selected) {
            document.getSelection().removeAllRanges()
            document.getSelection().addRange(selected)
        }
        if (result === "unsuccessful") console.error("Failed to copy text.")
    }

    return (
        <>
            <form onSubmit={onSubmit}>
                <div className="file">
                    <label className="file-label">
                        <input className="file-input" type="file" name="asset" id="asset" />
                        <span className="file-cta">
                            <span className="file-icon">
                                <i className="fas fa-file-upload"></i>
                            </span>
                            <span className="file-label">Choose a file…</span>
                        </span>
                    </label>
                </div>
                <button type="submit" className="button is-primary">
                    Upload
                </button>
            </form>
            {assets ? (
                <ul>
                    {assets.map((name, idx) => {
                        return (
                            <li key={idx}>
                                <button
                                    className="button is-white"
                                    onClick={e => {
                                        onAssetDelete(e, name)
                                    }}>
                                    <i className="fas fa-times"></i>
                                </button>
                                <button
                                    className="button is-white"
                                    onClick={e => {
                                        handleAssetClick(e, name)
                                    }}>
                                    {name}
                                </button>
                                <button
                                    className="button is-white"
                                    onClick={e => {
                                        handleAssetCopy(e, name)
                                    }}>
                                    <i className="fas fa-clipboard"></i>
                                </button>
                            </li>
                        )
                    })}
                </ul>
            ) : null}
            <CloseOnEscape onEscape={handleClose}>
                <Modal id={id} asset={asset} modalState={modalState} closeModal={handleClose} />
            </CloseOnEscape>
        </>
    )
}
