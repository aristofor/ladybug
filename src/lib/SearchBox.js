import React, { useState } from "react"
import { useNavigate } from "react-router-dom"

export default function SearchBox() {
    const [q, setQ] = useState("")
    const navigate = useNavigate()

    const handleSearch = e => {
        e.preventDefault()
        navigate("/search?q=" + encodeURIComponent(q))
    }

    return (
        <form onSubmit={handleSearch}>
            <p className="control has-icons-right">
                <input
                    className="input"
                    type="text"
                    name="q"
                    placeholder="Search..."
                    onChange={e => {
                        setQ(e.target.value)
                    }}
                />
                <span className="icon is-right">
                    <i className="fas fa-search"></i>
                </span>
            </p>
        </form>
    )
}
