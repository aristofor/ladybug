import React from "react"
import TagsInput from "./TagsInput"

export default function EditForm(props) {
    return (
        <form onSubmit={props.onSubmit} onReset={props.onReset}>
            <div className="field">
                <label className="label">Title</label>
                <input
                    autoFocus
                    className="input is-medium"
                    type="text"
                    spellCheck="false"
                    defaultValue={props.title}
                    onChange={e => {
                        props.setTitle(e.target.value)
                    }}
                />
            </div>
            <div className="field">
                <label className="label">Tags</label>
                <TagsInput tags={props.tags} onChange={props.setTags} inputProps={{ placeholder: "tag" }} />
            </div>
            <div className="field">
                <label className="label">Body</label>
                <textarea
                    className="textarea"
                    rows="20"
                    style={{ fontFamily: "monospace" }}
                    spellCheck="false"
                    defaultValue={props.body}
                    onChange={e => {
                        props.setBody(e.target.value)
                    }}></textarea>
            </div>
            {props.children}
        </form>
    )
}
