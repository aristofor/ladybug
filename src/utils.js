export const makeColumns = (cols, items) => {
    /*
     * Construit et équilibre des colonnes
     * à partir d'une liste d'éléments
     */
    const result = []
    const page_total = items.length
    const items_per_col = Math.floor(page_total / cols)
    for (let i = 0; i < cols; ++i) {
        let cur_col = []
        for (let j = 0; j < items_per_col; ++j) {
            let item = items.shift()
            if (item) cur_col.push(item)
            else break
        }
        if (i < page_total % cols) cur_col.push(items.shift())
        result.push(cur_col)
    }
    return result
}
