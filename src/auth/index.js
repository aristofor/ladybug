import React, { createContext, useContext, useEffect, useMemo, useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"

const USERINFO_KEY = "LADYBUG_USERINFO"

fetch("/api/auth-info")
    .then(r => r.json())
    .then(data => {
        if (data) window.localStorage.setItem(USERINFO_KEY, JSON.stringify(data))
        else window.localStorage.removeItem(USERINFO_KEY)
    })

export let authFetch = fetch

export const AuthContext = createContext()

export const AuthContextProvider = ({ children }) => {
    const [userInfo, setUserInfo] = useState(JSON.parse(window.localStorage.getItem(USERINFO_KEY)) || null)
    const navigate = useNavigate()
    const location = useLocation()

    const _authFetch = (url, options) => {
        const fail = () => {
            setUserInfo(null)
            navigate("/login", { replace: true, state: { next: location } })
            return fetch("/api/logout")
        }
        return fetch(url, { credentials: "same-origin", ...options }).then(r => {
            if (r.status === 401) fail()
            return r
        })
    }

    authFetch = _authFetch

    const isAdmin = useMemo(() => {
        if (!userInfo?.roles) return false
        return userInfo.roles.includes("admin")
    }, [userInfo])

    useEffect(() => {
        if (userInfo) window.localStorage.setItem(USERINFO_KEY, JSON.stringify(userInfo))
        else window.localStorage.removeItem(USERINFO_KEY)
    }, [userInfo])

    const defaultContext = {
        userInfo,
        setUserInfo,
        isAdmin
    }

    return <AuthContext.Provider value={defaultContext}>{children}</AuthContext.Provider>
}

export const useAuth = () => {
    const authContext = useContext(AuthContext)
    return authContext.userInfo
}
