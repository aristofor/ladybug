import React, { useState, useContext } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import { AuthContext } from "./index"

export default function Login() {
    const [formData, setFormData] = useState({
        username: "",
        password: ""
    })

    const authContext = useContext(AuthContext)
    const [message, setMessage] = useState("")

    const location = useLocation()
    const navigate = useNavigate()

    const onChange = evt => {
        setFormData({ ...formData, [evt.target.name]: evt.target.value })
    }

    const handleSubmit = evt => {
        evt.preventDefault()
        setMessage("")
        fetch("/api/login", { method: "POST", body: JSON.stringify(formData) })
            .then(r => {
                if (r.status === 401) {
                    setMessage("Échec")
                    return null
                } else return r.json()
            })
            .then(data => {
                if (data) {
                    authContext.setUserInfo(data)
                    const next = location?.state?.next
                    navigate(next ? next : "/", { replace: true })
                }
            })
    }

    const handleReauthenticate = evt => {
        evt.preventDefault()
        authContext.setUserInfo(null)
    }

    return (
        <div className="hero is-medium">
            <div className="hero-body">
                <div className="columns is-centered">
                    <div className="column is-4">
                        <h2 className="title">Login</h2>
                        {!authContext.userInfo ? (
                            <>
                                <form onSubmit={handleSubmit}>
                                    <div className="field">
                                        <label className="label">Username</label>
                                        <input
                                            autoFocus
                                            name="username"
                                            type="text"
                                            value={formData.username}
                                            onChange={onChange}
                                            className="input"
                                        />
                                    </div>
                                    <div className="field">
                                        <label className="label">Password</label>
                                        <input
                                            name="password"
                                            type="password"
                                            value={formData.password}
                                            onChange={onChange}
                                            className="input"
                                        />
                                    </div>
                                    <br />
                                    {message.length > 0 && <div className="notification is-danger">{message}</div>}
                                    <div className="field is-grouped">
                                        <div className="control">
                                            <input type="submit" value="Login" className="button is-primary" />
                                        </div>
                                        <div className="control">
                                            <button
                                                className="button is-light"
                                                onClick={e => {
                                                    e.preventDefault()
                                                    navigate("/recover")
                                                }}>
                                                Forgot password?
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </>
                        ) : (
                            <button onClick={handleReauthenticate} type="submit" className="button">
                                Reconnexion
                            </button>
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}
