import { useContext, useEffect } from "react"
import { AuthContext } from "../auth"
import { Navigate } from "react-router-dom"

export default function Logout() {
    const authContext = useContext(AuthContext)

    useEffect(() => {
        authContext.setUserInfo(null)
        fetch("/api/logout")
        // eslint-disable-next-line
    }, [])

    return <Navigate to="/" replace={true} />
}
