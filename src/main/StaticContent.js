import React, { useEffect, useState } from "react"
import { authFetch } from "../auth"
import Loading from "../lib/Loading"

export default function StaticContent(props) {
    const [html, setHtml] = useState("")
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)

    useEffect(() => {
        setLoading(true)
        setError(false)
        authFetch(`/api/${props.slug}`)
            .then(r => r.json())
            .then(data => {
                document.title = data.title
                setHtml(data.html)
            })
            .catch(error => {
                setError(true)
                console.log("error", error)
            })
            .finally(() => {
                setLoading(false)
            })
    }, [props.slug])

    if (loading) return <Loading />

    if (error) return <div>Erreur</div>

    return (
        <>
            <div className="content" dangerouslySetInnerHTML={{ __html: html }}></div>
        </>
    )
}
