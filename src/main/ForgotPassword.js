import React, { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"

export default function ForgotPassword() {
    const [email, setEmail] = useState("")
    const [error, setError] = useState(false)
    const [message, setMessage] = useState("")
    const navigate = useNavigate()

    useEffect(() => {
        document.title = "Recover account"
    }, [])

    const handleSubmit = event => {
        event.preventDefault()
        fetch("/api/recover/" + encodeURIComponent(email))
            .then(r => r.json())
            .then(data => {
                if (data.ok) navigate("/recover/" + encodeURIComponent(email))
                else {
                    setError(true)
                    setMessage(data.msg)
                }
            })
            .catch(error => {
                console.log("error", error)
            })
    }

    return (
        <section className="hero is-halfheight">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column is-offset-4 is-4">
                            <h1 className="title">Recover account</h1>
                            <form onSubmit={handleSubmit}>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">
                                            E-mail
                                            <input
                                                autoFocus
                                                className="input"
                                                defaultValue={email}
                                                onChange={e => {
                                                    setEmail(e.target.value)
                                                }}
                                            />
                                        </label>
                                    </div>
                                </div>

                                <div className="field">
                                    <div className="control">
                                        <input type="submit" value="Send code" className="button is-primary" />
                                    </div>
                                </div>
                            </form>
                            {error ? (
                                <>
                                    <br />
                                    <div className="notification is-danger">{message}</div>
                                </>
                            ) : (
                                ""
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
