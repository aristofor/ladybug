import React, { useState, useContext, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { AuthContext } from "../auth"

export default function ForgotPasswordToken({ match }) {
    const authContext = useContext(AuthContext)
    const [token, setToken] = useState("")
    const [error, setError] = useState(false)
    const [message, setMessage] = useState("")
    const navigate = useNavigate()

    useEffect(() => {
        document.title = "Recover code"
    }, [])

    const handleSubmit = event => {
        event.preventDefault()
        const email = decodeURI(match.params.email)
        const requestOptions = {
            method: "POST",
            body: JSON.stringify({ token: token })
        }
        fetch("/recover/" + email, requestOptions)
            .then(r => r.json())
            .then(data => {
                if (data.ok) {
                    const info = {
                        username: data.username,
                        roles: data.roles ? data.roles : []
                    }
                    authContext.setUserInfo(info)
                    navigate("/password")
                } else {
                    setError(true)
                    setMessage(data.msg)
                }
            })
    }

    return (
        <section className="hero is-halfheight">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column is-offset-4 is-4">
                            <h1 className="title">Confirmation code</h1>
                            {error ? <div className="notification is-danger">{message}</div> : ""}
                            <form onSubmit={handleSubmit}>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">
                                            Code
                                            <input
                                                autoFocus
                                                className="input"
                                                defaultValue={token}
                                                onChange={e => {
                                                    setToken(e.target.value)
                                                }}
                                            />
                                        </label>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <input type="submit" value="Submit" className="button is-primary" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
