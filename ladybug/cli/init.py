import click
from os.path import isfile, join
from textwrap import dedent
from ..database import db
from ._common import app, common_options


@click.group()
def cli():
    """
    Init stuff
    """
    pass


@cli.command('db')
@common_options
def init_db():
    """
    Creates tables
    """
    with app.app_context():
        db.create_all()
        app.logger.info('Tables created.')


@cli.command()
@common_options
def index_html():
    """
    Render var/public/index.html
    """
    with app.app_context():
        app.state.render_index_html()
        app.logger.info('Render index.html.')


@cli.command()
@click.option('-y', '--yes', is_flag=True, default=False, help='Answer yes to questions.')
@common_options
def static(yes):
    with app.app_context():
        fname = join(app.var_dir, 'data', 'home.md')
        write = True
        if not yes and isfile(fname):
            write = click.confirm("Overwrite {}".format(fname))
        if write:
            text = """\
                <div class="hero">
                <div class="hero-body" style="text-align:center">
                <img src="/static/media/logo-react.svg" class="App-logo" alt="logo" />
                <p>
                  Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                  href="https://reactjs.org"
                  target="_blank"
                  rel="noopener noreferrer"
                  class="button"
                >
                  Learn React <span class="icon-link"></span>
                </a>
                </div>
                </div>
                """
            with open(fname, 'w') as fd:
                fd.write(dedent(text))
            app.logger.info(fname)

        fname = join(app.var_dir, 'data', 'about.md')
        write = True
        if not yes and isfile(fname):
            write = click.confirm("Overwrite {}".format(fname))
        if write:
            text = """\
                <div class="hero">
                <div class="hero-body" style="text-align:center">
    
                <img src="/static/media/logo-ladybug.svg" class="App-logo" alt="logo" />
    
                LadyBug
                =======
    
                by Aristofor Kolomb
                <br/>&copy; 2020-2023 Association Bug
    
                [source](https://gitlab.com/aristofor/ladybug){target="_blank"}
    
                </div>
                </div>
                """
            with open(fname, 'w') as fd:
                fd.write(dedent(text))
            app.logger.info(fname)


if __name__ == '__main__':
    cli()
