import click
from os import chdir, listdir, makedirs, unlink
from os.path import isdir, join, split
from shutil import rmtree
import tarfile
from uuid import UUID
import sqlalchemy.exc
from datetime import datetime
from ..database import db
from ..kb.models import Content, Tag
from ..kb.asset import asset_rmdir_if_empty, shard
from ..utils import parse_yaml_body
from ._common import app, common_options

###############################################################################


class PolicyBase(object):

    def initialize(self):
        pass

    def insert(self, data):
        pass

    def finalize(self):
        pass


class BlankPolicy(PolicyBase):

    def initialize(self):
        try:
            Tag.__table__.drop(db.engine)
        except sqlalchemy.exc.ProgrammingError:
            pass  # la table n'existe pas
        try:
            Content.__table__.drop(db.engine)
        except sqlalchemy.exc.ProgrammingError:
            pass  # la table n'existe pas
        Content.__table__.create(db.engine)
        Tag.__table__.create(db.engine)
        assets_dir = join(app.var_dir, 'assets')
        for dname in listdir(assets_dir):
            rmtree(join(assets_dir, dname))
        Content.mtime_auto = False

    def insert(self, data):
        obj = Content(id=UUID(data['id']))
        app.logger.info('insert {}'.format(obj.id.hex))
        obj.title = data['title']
        obj.mtime = datetime.fromtimestamp(data['mtime'])
        obj.body = data['body']
        for p, t in enumerate(data['tags']):
            obj.tags.append(Tag(rep=t, pri=p))
        db.session.add(obj)
        db.session.commit()


class UpdatePolicy(PolicyBase):

    def __init__(self):
        super().__init__()
        self.update_list = []
        self.has_assets = {}

    def initialize(self):
        Content.mtime_auto = False

    def insert(self, data):
        obj = Content.query.filter_by(id=data['id']).first()
        mtime = datetime.fromtimestamp(data['mtime'])
        if obj is None:
            dirty = True
            obj = Content(id=UUID(data['id']))
            app.logger.info('insert {}'.format(obj.id.hex))
        elif obj.mtime < mtime:
            dirty = True
            app.logger.info('update {}'.format(obj.id.hex))
        else:
            dirty = False
        if dirty:
            obj.title = data['title']
            obj.mtime = mtime
            obj.body = data['body']
            obj.tags.clear()
            for p, t in enumerate(data['tags']):
                obj.tags.append(Tag(rep=t, pri=p))
            db.session.add(obj)
            db.session.commit()
            self.update_list.append(obj.id.hex)

    def remove_assets(self, id):
        dname = join(app.var_dir, 'assets', shard(id))
        for fname in listdir(dname):
            unlink(join(dname, fname))
        asset_rmdir_if_empty(dname)

    def finalize(self):
        for k in self.has_assets.keys():
            if k in self.update_list:
                self.update_list.remove(k)
        """
        Le reste présent dans update_list n'a pas d'asset, on efface
        """
        for id in self.update_list:
            self.remove_assets(id)


###############################################################################


@click.command()
@click.option('--merge/--blank', default=True, help="Fusionne / Remet à zéro")
@click.argument('archname', type=click.Path(exists=True))
@common_options
def cli(merge, archname):
    """
    Importe des contenus
    """
    with app.app_context():
        if merge:
            policy = UpdatePolicy()
        else:
            policy = BlankPolicy()
        policy.initialize()
        with tarfile.open(archname, 'r:bz2') as tar:
            for tarfname in tar.getnames():
                app.logger.debug(tarfname)
                dname, fname = split(tarfname)
                if dname == '':
                    with tar.extractfile(tarfname) as fd:
                        meta, body = parse_yaml_body(fd.read())
                        policy.insert(dict(body=body, **meta))
                else:
                    absdname = join(app.var_dir, 'assets', shard(dname))
                    path = join(absdname, fname)
                    if not isdir(absdname):
                        makedirs(absdname)
                    if merge:
                        """
                        Maintient la liste des éléments mis
                        à jour contenant des assets
                        """
                        policy.has_assets[dname] = True
                    else:
                        """
                        blank, ajoute tous les fichiers
                        """
                        pass
                    """
                    normalement, la fonction
                        tar.extract(tarfname,path)
                    devrait faire l'affaire
                    mais elle a tendance à créer des répertoires
                    au lieu d'extraire le contenu de l'archive.
                    """
                    with tar.extractfile(tarfname) as fd:
                        with open(path, 'wb') as fb:
                            fb.write(fd.read())
        policy.finalize()


if __name__ == '__main__':
    cli()
