import click
from flask.cli import with_appcontext
from os import chdir, mkdir, listdir
from os.path import basename, dirname, isdir, join
import tempfile
import tarfile
import shutil
from datetime import datetime
import platform
import getpass
from ..utils import content_dump
from ..kb.models import Content
from ._common import app, common_options

#######################################################################################################################


def find_files(topdir):
    stack = [topdir]
    while 1:
        try:
            entry = stack.pop()
            for base in listdir(entry):
                name = join(entry, base)
                if isdir(name):
                    stack.append(name)
                else:
                    yield name
        except IndexError:
            break


def dump_assets(path, archive):
    """
    Ajoute les assets à l'archive, en chemin non-fragmenté.
    """
    for fname in find_files(path):
        unsharded = join(basename(dirname(fname)), basename(fname))
        app.logger.debug(unsharded)
        archive.add(fname, unsharded)


#######################################################################################################################


@click.command()
@common_options
def cli():
    """
    Exporte les contenus
    """
    with app.app_context():
        dump_dir = join(app.var_dir, 'dump')
        if not isdir(dump_dir):
            app.logger.debug("mkdir {}".format(dump_dir))
            mkdir(dump_dir)

        with tempfile.TemporaryDirectory(prefix='ladybug') as tmpdirname:
            chdir(tmpdirname)
            archname = '{}-{}-{}.tar.bz2'.format(platform.node(), getpass.getuser(),
                                                 datetime.now().strftime('%Y%m%d-%H%M%S'))
            archive = tarfile.open(archname, 'w:bz2')
            for model in Content.query.all():
                dumpfname = f'{model.id.hex}.yml'
                app.logger.info(dumpfname)
                content_dump(model, dumpfname)
                archive.add(dumpfname)
            dump_assets(join(app.var_dir, 'assets'), archive)
            archive.close()
            shutil.move(archname, join(dump_dir, archname))
            click.echo(f'archive: {archname}')


if __name__ == '__main__':
    cli()
