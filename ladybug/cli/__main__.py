"""
basé sur:
https://github.com/pallets/click/blob/master/examples/complex/complex/cli.py
"""

from pathlib import Path
import click
from ..app import app
from ._common import common_options

cmd_folder = Path(__file__).parent


class ComplexCLI(click.MultiCommand):

    def list_commands(self, ctx):
        rv = list()
        for fname in cmd_folder.iterdir():
            if not fname.name.startswith('_'):
                rv.append(fname.stem)
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            mod = __import__(f'{__package__}.{name}', None, None, ['cli'])
        except ImportError as exc:
            print(exc)
            return
        return mod.cli


@click.command(cls=ComplexCLI)
@common_options
def cli():
    """LadyBug management cli"""


if __name__ == '__main__':
    cli()
