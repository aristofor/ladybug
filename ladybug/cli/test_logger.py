import click
from flask.cli import with_appcontext
from ._common import app, common_options


@click.group()
def cli():
    """
    Test command
    """
    pass


@cli.command()
@common_options
def dummy():
    """
    Logger test
    """
    with app.app_context():
        app.logger.debug("Debug message")
        app.logger.info("Info message")
        app.logger.warn("Warn message")
        app.logger.error("Error message")


if __name__ == '__main__':
    cli()
