import click
from flask.cli import with_appcontext
from datetime import datetime
from os.path import isdir, join
import sys
import csv
from ..database import db
from ..account.models import Account
from ._common import app, common_options


@click.group()
def cli():
    """
    Gère les comptes
    """
    pass


@cli.command()
@common_options
def dump():
    """
    Exporte les comptes
    """
    with app.app_context():
        dump_dir = join(app.var_dir, 'dump')
        if not isdir(dump_dir):
            app.logger.debug(f"mkdir {dump_dir}")
            mkdir(dump_dir)
        fname = join(dump_dir, 'accounts-{}.csv'.format(datetime.now().strftime('%Y%m%d-%H%M%S')))
        click.echo(fname)
        with open(fname, 'w') as fd:
            fieldnames = ('username', 'email', 'password_hash', 'admin')
            writer = csv.DictWriter(fd, fieldnames=fieldnames, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writeheader()
            for row in Account.query.all():
                app.logger.info(f"write account '{row.username}'")
                writer.writerow({
                    'username': row.username,
                    'email': row.email,
                    'password_hash': row.password_hash,
                    'admin': int(row.admin)
                })


@cli.command('import')
@click.argument('filename')
@common_options
def import_csv(filename):
    """
    Importe les comptes depuis le fichier csv.
    """
    with app.app_context():
        db.drop_all(bind='accounts')
        db.create_all(bind='accounts')
        with open(filename, 'r') as fd:
            reader = csv.DictReader(fd, delimiter=';', quotechar='"')
            for row in reader:
                obj = Account(username=row['username'],
                              email=row['email'],
                              password_hash=row['password_hash'],
                              admin=int(row['admin']))
                app.logger.info("import account '{}'".format(obj.username))
                db.session.merge(obj)
                db.session.commit()


@cli.command()
def list():
    """
    Liste des comptes
    """
    with app.app_context():
        click.echo("  {:<20} {}".format("username", "email"))
        for row in Account.query.all():
            click.echo("{} {:<20} {}".format('*' if row.admin else ' ', row.username, row.email))


@cli.command()
def create():
    """
    Crée un compte
    """
    with app.app_context():
        username = click.prompt("username")
        email = click.prompt("e-mail")
        password = click.prompt("password")
        admin = click.confirm("admin")
        obj = Account(username=username, email=email, admin=admin)
        obj.set_password(password)
        db.session.add(obj)
        db.session.commit()


@cli.command()
@click.argument('username')
def delete(username):
    """
    Efface un compte
    """
    with app.app_context():
        obj = Account.query.filter(Account.username == username).first()
        if obj is None:
            click.echo("Aucun compte trouvé.")
            sys.exit(1)
        if click.confirm("Vraiment?"):
            db.session.delete(obj)
            db.session.commit()
            click.echo("Compte effacé.")
        else:
            click.echo("Abandon")


@cli.command()
@click.argument('username')
def edit(username):
    """
    Édite un compte
    """
    with app.app_context():
        obj = Account.query.filter(Account.username == username).first()
        if obj is None:
            click.echo("Aucun compte trouvé.")
            sys.exit(1)
        username = click.prompt("username", default=obj.username)
        if username != obj.username:
            obj.username = username
        email = click.prompt("e-mail", default=obj.email)
        if email != obj.email:
            obj.email = email
        password = click.prompt("password", default='')
        if password != '':
            obj.set_password(password)
        obj.admin = click.confirm("admin", default=obj.admin)
        if db.session.dirty:
            db.session.merge(obj)
            db.session.commit()
            click.echo("Account saved.")


###############################################################################

if __name__ == '__main__':
    cli()
