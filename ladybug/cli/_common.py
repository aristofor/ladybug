"""
basé sur :
    https://github.com/pallets/click/issues/108
"""

import click
from ..app import app


class State(object):

    def __init__(self):
        self.verbosity = 0


def verbosity_option(f):

    def callback(ctx, param, value):
        state = ctx.ensure_object(State)
        value = max(0, min(3, value))
        state.verbosity = value
        level = ('ERROR', 'WARNING', 'INFO', 'DEBUG')[value]
        with app.app_context():
            app.logger.setLevel(level)
        return value

    return click.option('-v',
                        '--verbose',
                        count=True,
                        expose_value=False,
                        help='Enables verbosity.',
                        callback=callback)(f)


def common_options(f):
    f = verbosity_option(f)
    return f
