from os.path import join
import click
from flask.cli import with_appcontext
from glob import glob
from os import stat, unlink
from os.path import isdir, join
from shutil import rmtree
from datetime import datetime, timedelta
from ._common import app, common_options


@click.group()
def cli():
    """
    Trash management
    """
    pass


@cli.command()
@click.argument('days', type=int)
@common_options
def flush(days):
    """
    Flush trash items older than DAYS argument.
    """
    with app.app_context():
        trash_dir = join(app.var_dir, 'trash')
        delta = timedelta(days=days)
        bound = datetime.now() - delta
        for fname in glob(join(trash_dir, '*.yml')):
            t = datetime.fromtimestamp(stat(fname).st_mtime)
            if t < bound:
                canon = fname[:-4]
                if isdir(canon):
                    app.logger.debug(f"rmtree {canon}")
                    rmtree(canon)
                app.logger.debug(f"unlink {fname}")
                unlink(fname)


###############################################################################

if __name__ == '__main__':
    cli()
