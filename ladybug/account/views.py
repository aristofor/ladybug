from flask import Blueprint, current_app, request
from random import choice
from flask_login import current_user, login_user
from flask_mail import Message
from urllib.parse import quote, unquote
from ..database import db
from .models import Account

mod = Blueprint('account', __name__)

# préfixe pour la clé de cache de récupération
CACHE_RECOVER_KEY = 'recover:'
# durée de vie d'un code de récupération
CACHE_RECOVER_DURATION = 60 * 15

from jinja2 import Environment, PackageLoader, select_autoescape

jinja_mail_env = Environment(loader=PackageLoader(__package__, 'mail'),
                             autoescape=select_autoescape(disabled_extensions=('txt',),
                                                          default_for_string=True,
                                                          default=True))

#######################################################################################################################


def generate_token():
    alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return ''.join(choice(alphabet) for x in range(6))


#######################################################################################################################

from threading import Thread


def send_mail_async(app, msg):
    with app.app_context():
        app.mail.send(msg)


@mod.get('/recover/<string:email>')
def recover(email):
    email = unquote(email)
    account = Account.query.filter(Account.email == email).first()
    if account is None:
        return jsonify(ok=False, msg="Unknown e-mail.")
    cache_key = CACHE_RECOVER_KEY + email
    token = generate_token()
    current_app.cache.set(cache_key, token, timeout=CACHE_RECOVER_DURATION)
    msg = Message("LadyBug: Recover account", recipients=[email])
    title = "LadyBug recover account"
    if current_app.debug:
        link_format = '{}/{}'
        link = link_format.format(request.environ['HTTP_REFERER'], quote(email))
    else:
        link_format = '{}://{}/recover/{}'
        link = link_format.format(request.environ['wsgi.url_scheme'], request.environ['HTTP_HOST'], quote(email))
    tpl_args = dict(title=title, link=link, token=token)
    msg.body = jinja_mail_env.get_template('recover.txt').render(**tpl_args)
    msg.html = jinja_mail_env.get_template('recover.html').render(**tpl_args)
    #current_app.mail.send(msg)
    Thread(target=send_mail_async, args=(current_app._get_current_object(), msg)).start()
    return dict(ok=True, msg="Mail sent to <{}>".format(email))


@mod.post('/recover/<string:email>')
def recover_token(email):
    email = unquote(email)
    account = Account.query.filter(Account.email == email).first()
    if account is None:
        return dict(ok=False, msg="Unknown e-mail.")
    req_data = request.get_json(force=True)
    req_token = req_data.get('token', None)
    if req_token is None:
        abort(400)
    cache_key = CACHE_RECOVER_KEY + email
    cache_token = current_app.cache.get(cache_key)
    if cache_token is None:
        return dict(ok=False, msg="No token.")
    if cache_token == req_token:
        login_user(account)
        current_app.cache.delete(cache_key)
        return dict(ok=True, username=account.username, roles=account.roles)
    return dict(ok=False, msg="Code doesn't match.")


#######################################################################################################################


@mod.post('/password')
def password():
    req_data = request.get_json(force=True)
    req_password = req_data.get('password', None)
    if req_password is None:
        abort(400)
    current_user.set_password(req_password)
    db.session.merge(current_user)
    db.session.commit()
    return dict(ok=True)
