from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from ..database import db

__all__ = ('Account',)


class Account(db.Model, UserMixin):

    __bind_key__ = 'accounts'

    username = db.Column(db.String(20), primary_key=True)
    email = db.Column(db.String(254), unique=True)
    password_hash = db.Column(db.String(94), default=None)
    admin = db.Column(db.Boolean, default=False)

    def get_id(self):
        return self.username

    def set_password(self, pw):
        self.password_hash = generate_password_hash(pw)

    def verify_password(self, pw):
        if self.password_hash is None:
            return False
        return check_password_hash(self.password_hash, pw)

    @property
    def roles(self):
        roles = list()
        if self.admin:
            roles.append('admin')
        return roles
