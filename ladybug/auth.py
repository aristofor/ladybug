from flask import Blueprint, current_app, request
from flask_login import LoginManager, current_user, login_user, logout_user
from werkzeug.security import check_password_hash
from flask_login import UserMixin
from .account.models import Account

mod = Blueprint('auth', __name__)
lm = LoginManager()


@lm.user_loader
def load_user(username):
    if username:
        account = Account.query.filter(Account.username == username).first()
        return account
    else:
        return lm.anonymous_user


@mod.get('/logout')
def logout():
    logout_user()
    return dict(msg="logged out")


@mod.post('/login')
def login():
    if not current_user.is_anonymous:
        logout_user()
    req_data = request.get_json(force=True)
    username = req_data.get('username', None)
    password = req_data.get('password', None)
    if username is None or password is None:
        abort(401)
    account = Account.query.filter(Account.username == username).first()
    if account and account.verify_password(password):
        login_user(account)
        roles = dict(roles=account.roles) if account.roles else dict()
        return dict(username=account.username, **roles)
    return dict(msg="error"), 401


@mod.get('/auth-info')
def info():
    if current_user.is_anonymous:
        return current_app.response_class('null', status=200, mimetype='application/json')
    else:
        roles = dict(roles=current_user.roles) if current_user.roles else dict()
        return dict(username=current_user.username, **roles)
