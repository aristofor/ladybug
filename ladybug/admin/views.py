from flask import abort, Blueprint, current_app, request
from flask.views import MethodView
from flask_login import current_user
from os import listdir
from os.path import join
from fnmatch import fnmatch

mod = Blueprint('admin', __name__)


@mod.before_request
def before_request():
    if not 'admin' in current_user.roles:
        abort(403)


@mod.get('clear_cache')
def clear_cache():
    current_app.cache.clear()
    return dict(ok=True, msg="Cache cleared.")


#######################################################################################################################


class LayoutAPI(MethodView):

    def get(self):
        """
        Valeur courante de AppState
        """
        layout = dict(current_app.state.items())
        themes = []
        for dname in listdir(join(current_app.static_folder, 'themes')):
            themes.append(dname)
        highlights = []
        for fname in listdir(join(current_app.static_folder, 'highlight')):
            if fnmatch(fname, '*.css'):
                highlights.append(fname[:-4])
        return dict(ok=True, layout=layout, themes=sorted(themes), highlights=sorted(highlights))

    def post(self):
        """
        Mise à jour de AppState
        """
        req_data = request.get_json(force=True)
        current_app.state.update(req_data)
        current_app.state.save()
        current_app.state.render_index_html()
        return dict(ok=True, msg="appstate saved.")


mod.add_url_rule('/layout', view_func=LayoutAPI.as_view('layout'), methods=['GET', 'POST'])

#######################################################################################################################
from os.path import basename, dirname, getmtime, isdir, isfile
from os import makedirs, rename, unlink
from shutil import rmtree
from glob import glob
from datetime import datetime
from ..utils import parse_yaml_body
from ..kb.models import Content, Tag
from ..database import db
from ..kb.asset import shard


@mod.get('/trash')
def trash():
    """
    Liste les éléments effacés
    """
    dname = join(current_app.var_dir, 'trash')
    items = []
    for entry in glob(join(dname, '*.yml')):
        mtime = getmtime(entry)
        canon = basename(entry)[:-4]
        items.append({'id': canon, 'assets': isdir(join(dname, canon)), 'mtime': mtime})
    result = sorted(items, key=lambda item: item['mtime'])
    for item in result:
        item['mtime'] = datetime.fromtimestamp(item['mtime']).strftime("%F %T")
    return dict(ok=True, trash=result)


@mod.get('/trash/item/<string:id>/restore')
def trash_restore(id):
    """
    Resaure un élément effacé
    """
    canon = join(join(current_app.var_dir, 'trash', id))
    fname = canon + ".yml"
    if not isfile(fname):
        abort(404)
    Content.query.filter(Content.id == id).delete()
    db.session.commit()
    with open(fname, 'r') as fd:
        data, body = parse_yaml_body(fd.read())
    if isdir(canon):
        assets_dir = join(current_app.var_dir, 'assets', shard(id))
        makedirs(dirname(assets_dir))
        rename(canon, dest_dir)
    obj = Content(id=id, title=data['title'], body=body, mtime=datetime.fromtimestamp(data['mtime']), mtime_auto=False)
    db.session.add(obj)
    # nécéssaire sinon whoosh lancera une KeyError à la prochaine recherche
    db.session.commit()
    for p, t in enumerate(data['tags']):
        obj.tags.append(Tag(rep=t, pri=p))
    db.session.commit()
    unlink(fname)
    return dict(ok=True, msg="#" + id + " restored.")


class TrashItemAPI(MethodView):

    def get(self, id):
        fname = join(join(current_app.var_dir, 'trash', id + ".yml"))
        if not isfile(fname):
            abort(404)
        with open(fname, 'r') as fd:
            text = fd.read()
        return dict(ok=True, text=text)


mod.add_url_rule('/trash/item/<string:id>', view_func=TrashItemAPI.as_view('trash_item'), methods=['GET'])
