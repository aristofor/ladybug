from os import listdir, rmdir
from os.path import dirname, join
from uuid import UUID


def shard(value):
    if isinstance(value, UUID):
        hexa = value.hex
    elif isinstance(value, str):
        hexa = value
    else:
        raise ValueError
    return join(hexa[-1], hexa[-2], hexa)


def asset_rmdir_if_empty(dname, rec=3):
    if not rec:
        return
    if not listdir(dname):
        rmdir(dname)
        asset_rmdir_if_empty(dirname(dname), rec - 1)
