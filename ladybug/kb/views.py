from flask import abort, Blueprint, current_app, request
from flask.views import MethodView
from sqlalchemy import func
from uuid import UUID, uuid4
from os.path import isdir, isfile, join
from os import makedirs, unlink
from werkzeug.utils import secure_filename
from shutil import copy, copytree, rmtree
from ..database import db
from .models import Content, Tag
from ..renderer import render_markdown
from .asset import asset_rmdir_if_empty, shard
from ..utils import content_dump

mod = Blueprint('kb', __name__)

#######################################################################################################################


@mod.get('/contents')
def contents():
    """
    Liste des contenus
    """
    page = int(request.args.get('page', 0)) or 1
    psize = current_app.state['page_rows'] * current_app.state['page_cols']
    pagination = Content.query.order_by(Content.mtime.desc()).paginate(page=page, per_page=psize, count=True)
    results = [dict(id=item.id.hex, title=item.title, tags=[str(x) for x in item.tags]) for item in pagination.items]
    return dict(items=results,
                page=pagination.page,
                columns=current_app.state['page_cols'],
                perPage=pagination.per_page,
                total=pagination.total)


@mod.get('/search')
def search():
    """
    Recherche full text
    """
    q = request.args.get('q')
    page = int(request.args.get('page', 0)) or 1
    psize = current_app.state['page_rows'] * current_app.state['page_cols']
    tsquery = func.plainto_tsquery("french", q)
    qs = Content.query.filter(Content.tsv.bool_op('@@')(tsquery)).order_by(func.ts_rank(Content.tsv, tsquery).desc())
    pagination = qs.paginate(page=page, per_page=psize, count=True)
    results = [dict(id=item.id.hex, title=item.title, tags=[str(x) for x in item.tags]) for item in pagination.items]
    return dict(items=results,
                page=pagination.page,
                columns=current_app.state['page_cols'],
                perPage=pagination.per_page,
                total=pagination.total)


class ContentAPI(MethodView):

    def get(self, id):
        """
        Affiche un contenu
        """
        try:
            id = UUID(id)
        except ValueError:
            abort(404)
        model = Content.query.get(id)
        if model is None:
            abort(404)
        data = dict(id=model.id.hex,
                    title=model.title,
                    mtime=model.mtime.strftime("%F %T"),
                    tags=[str(x) for x in model.tags])
        assets = request.args.get('assets', None)
        if assets:
            data['assets'] = model.assets
        raw = request.args.get('raw', None)
        if raw:
            data['body'] = model.body
        else:
            data['html'] = render_markdown(model.body, model.id)
        return data

    @staticmethod
    def sanitize_tags(tags):
        """
        Strip les tags, enlève les doublons, enlève les vides
        """
        res = [x.strip() for x in tags]
        res = set([x for x in filter(None, res)])
        return list(res)

    def put(self, id):
        """
        Édite un contenu
        """
        try:
            id = UUID(id)
        except ValueError:
            abort(404)
        model = Content.query.get(id)
        if model is None:
            abort(404)
        req_data = request.get_json(force=True)
        model.title = req_data.get('title', "")
        model.body = req_data.get('body', "")
        model.tags.clear()
        db.session.merge(model)
        tags = self.sanitize_tags(req_data.get('tags', []))
        for p, rep in enumerate(tags):
            tag = Tag(content_id=model.id, rep=rep, pri=p)
            db.session.add(tag)
        db.session.commit()
        return dict(ok=True, id=model.id.hex)

    def post(self):
        """
        Crée un contenu
        """
        id = uuid4()
        model = Content(id=id)
        req_data = request.get_json(force=True)
        model.title = req_data.get('title', "")
        model.body = req_data.get('body', "")
        db.session.add(model)
        tags = self.sanitize_tags(req_data.get('tags', []))
        for p, rep in enumerate(tags):
            tag = Tag(content_id=model.id, rep=rep, pri=p)
            db.session.add(tag)
        db.session.commit()
        return dict(ok=True, id=id.hex), 201

    def delete(self, id):
        """
        Efface un contenu
        Copie dans la corbeille
        """
        try:
            id = UUID(id)
        except ValueError:
            abort(404)
        model = Content.query.get(id)
        if model is None:
            abort(404)
        trashdir = join(current_app.var_dir, 'trash')
        dumpfname = '{}.yml'.format(model.id.hex)
        content_dump(model, join(trashdir, dumpfname))
        assetsdir = join(current_app.var_dir, 'assets', shard(model.id))
        trash_assetsdir = join(trashdir, model.id.hex)
        if isdir(trash_assetsdir):
            rmtree(trash_assetsdir)
        if isdir(assetsdir):
            copytree(assetsdir, trash_assetsdir, copy_function=copy)
        db.session.delete(model)
        db.session.commit()
        return dict(ok=True, id=id.hex)


content_view = ContentAPI.as_view('content')
mod.add_url_rule('/content/<string:id>', view_func=content_view, methods=['GET', 'PUT', 'DELETE'])
mod.add_url_rule('/content', view_func=content_view, methods=['POST'])

#######################################################################################################################


@mod.route('/content/upload', methods=['POST'], defaults=dict(id=None))
@mod.route('/content/<string:id>/upload', methods=['POST'])
def upload(id):
    """
    Ajoute un asset
    """
    if not request.files:
        abort(400)
    if id is None:
        id = uuid4()
        model = Content(id=id)
    else:
        id = UUID(id)
        model = Content.query.get(id)
        if model is None:
            abort(404)
    db.session.merge(model)
    db.session.commit()
    file = request.files['asset']
    filename = secure_filename(file.filename)
    dname = join(current_app.var_dir, 'assets', shard(model.id))
    if not isdir(dname):
        makedirs(dname)
    file.save(join(dname, filename))
    return dict(ok=True, id=id.hex)


@mod.route('/content/<string:id>/assets')
def assets(id):
    """
    Liste des assets
    """
    try:
        id = UUID(id)
    except ValueError:
        abort(404)
    model = Content.query.get(id)
    if model is None:
        abort(404)
    return dict(ok=True, id=id.hex, assets=model.assets)


@mod.route('/content/<string:id>/asset/<string:name>', methods=['DELETE'])
def asset_delete(id, name):
    """
    Efface un asset
    """
    try:
        id = UUID(id)
    except ValueError:
        abort(404)
    model = Content.query.get(id)
    if model is None:
        abort(404)
    model.touch()
    db.session.merge(model)
    db.session.commit()
    dname = join(current_app.var_dir, 'assets', shard(id))
    fname = join(dname, name)
    if not isfile(fname):
        abort(404)
    unlink(fname)
    asset_rmdir_if_empty(dname)
    return dict(ok=True)


#######################################################################################################################


@mod.route('/tags')
def tags():
    """
    Liste des tags
    """
    page = int(request.args.get('page', 0)) or 1
    psize = current_app.state['tags_rows'] * current_app.state['tags_cols']
    pagination = Tag.query.with_entities(Tag.rep,func.count(Tag.rep)) \
        .group_by(Tag.rep).order_by(Tag.rep.asc()).paginate(page=page, per_page=psize, count=True)
    results = [dict(rep=item[0], count=item[1]) for item in pagination.items]
    return dict(items=results,
                page=pagination.page,
                columns=current_app.state['tags_cols'],
                perPage=pagination.per_page,
                total=pagination.total)


@mod.route('/tag/<path:rep>')
def tag(rep):
    """
    Liste des contenus taggé par rep
    """
    page = int(request.args.get('page', 0)) or 1
    psize = current_app.state['page_rows'] * current_app.state['page_cols']
    pagination = Content.query.join(Tag).filter(Tag.rep==rep). \
                order_by(Content.mtime.desc()).paginate(page=page, per_page=psize, count=True)
    results = [dict(id=item.id.hex, title=item.title, tags=[str(x) for x in item.tags]) for item in pagination.items]
    return dict(items=results,
                page=pagination.page,
                columns=current_app.state['page_cols'],
                perPage=pagination.per_page,
                total=pagination.total)
