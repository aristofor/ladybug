from flask import current_app
from datetime import datetime
from uuid import uuid4
from sqlalchemy import event, func
from os.path import isdir, join
from os import listdir, unlink
from sqlalchemy_utils import UUIDType
from sqlalchemy_utils.types.ts_vector import TSVectorType
from ..database import db
from .asset import asset_rmdir_if_empty, shard

__all__ = ('Content', 'Tag')

#######################################################################################################################


class Content(db.Model):
    """
    Contenu
    """
    id = db.Column(UUIDType, primary_key=True, default=uuid4)
    mtime = db.Column(db.DateTime, default=datetime.now, index=True)
    title = db.Column(db.String(80))
    body = db.Column(db.Text)
    tsv = db.Column(TSVectorType)

    __table_args__ = (db.Index("ix_content_tsv", tsv, postgresql_using='gin'),)

    #: Active la mise à jour automatique de mtime, vrai par défaut.
    mtime_auto = True

    def touch(self):
        """
        Mise à jour de mtime
        """
        self.mtime = datetime.now()

    @property
    def tags_str(self):
        """
        Représentation texte des tags, utilisée par l'indexeur
        """
        return '\n'.join([str(x) for x in self.tags])

    @property
    def assets(self):
        """
        Liste des assets
        """
        if self.id is None:
            return None
        dname = join(current_app.var_dir, 'assets', shard(self.id))
        if not isdir(dname):
            return []
        return sorted(listdir(dname))


@event.listens_for(Content, 'before_update')
@event.listens_for(Content, 'before_insert')
def content_before_insert(mapper, connection, target):
    """
    MAJ tsv et horodatage
    """
    target.tsv = func.to_tsvector('french', target.title + '\n' + target.body + '\n' + target.tags_str)
    if target.mtime_auto:
        target.touch()


@event.listens_for(Content, 'before_delete')
def content_before_delete(mapper, connection, target):
    """
    Efface les assets
    """
    dname = join(current_app.var_dir, 'assets', shard(target.id))
    if isdir(dname):
        for fname in listdir(dname):
            unlink(join(dname, fname))
        asset_rmdir_if_empty(dname)


#######################################################################################################################


class Tag(db.Model):
    """
    Tag record
    """
    content_id = db.Column(UUIDType, db.ForeignKey('content.id', ondelete='CASCADE'), primary_key=True)
    content = db.relationship('Content',
                              cascade='all, delete',
                              backref=db.backref('tags',
                                                 passive_deletes=True,
                                                 lazy='joined',
                                                 order_by='Tag.pri',
                                                 cascade='save-update, merge, delete, delete-orphan'))
    rep = db.Column(db.String(60), primary_key=True)
    pri = db.Column(db.SmallInteger)

    def __str__(self):
        return self.rep
