from yaml import CDumper as Dumper, dump, CSafeLoader as Loader, load
from datetime import datetime
from .kb.models import Content

#######################################################################################################################

Dumper.add_representer(
    datetime, lambda dumper, value: dumper.represent_scalar('tag:yaml.org,2002:float', str(value.timestamp())))

Dumper.add_representer(
    list, lambda dumper, value: dumper.represent_sequence('tag:yaml.org,2002:seq', value, flow_style=True))


def content_dump(model, dest):
    data = dict(id=model.id.hex, title=model.title, mtime=model.mtime, tags=[str(x) for x in model.tags])
    with open(dest, 'w') as fd:
        dump(data, stream=fd, Dumper=Dumper, allow_unicode=True, explicit_start=True, explicit_end=True)
        fd.write(model.body or '')


#######################################################################################################################


def parse_yaml_body(buff):
    if isinstance(buff, bytes):
        text = buff.decode('utf-8')
    else:
        text = buff
    if text.startswith('---'):
        end_meta = text.find('\n...\n')
        meta_text = text[:end_meta]
        if end_meta != -1:
            body = text[end_meta + 5:]
        else:
            body = ''
        meta = load(meta_text, Loader=Loader)
    else:
        body = text
        meta = {}
    return (meta, body)
