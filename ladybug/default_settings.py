JSON_SORT_KEYS = False
JSON_AS_ASCII = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
CACHE_KEY_PREFIX = 'cache:'

# pandoc 2.2.x : PANDOC_ARGS = ['--base-header-level=2']
# pandoc 2.9.x : PANDOC_ARGS = ['--shift-heading-level-by=1']
PANDOC_ARGS = []
