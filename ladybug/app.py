from flask import abort, Flask, request
from flask_login import current_user
from flask_caching import Cache
from werkzeug.exceptions import HTTPException
from flask_mail import Mail
from os.path import dirname, join
from .database import db
from .auth import mod as auth_module, lm
from .account.views import mod as account_module
from .kb.views import mod as kb_module
from .admin.views import mod as admin_module
from .static_content import StaticContent
from .app_state import AppState

app = Flask(__name__)
top = __name__.split('.')[0]
app.config.from_object(f'{top}.default_settings')
app.config.from_envvar('FLASK_SETTINGS')
setattr(app, 'var_dir', app.config['VAR_DIR'])

app.template_folder = join(app.var_dir, 'templates')

if app.debug:
    app.static_folder = join(dirname(app.root_path), 'build')
else:
    app.static_folder = join(app.var_dir, 'htdocs')

db.init_app(app)
lm.init_app(app)
cache = Cache(app)
setattr(app, 'cache', cache)
mail = Mail(app)
setattr(app, 'mail', mail)
AppState(app)

app.register_blueprint(auth_module, url_prefix='/api')
app.register_blueprint(account_module, url_prefix='/api')
app.register_blueprint(kb_module, url_prefix='/api')
app.register_blueprint(admin_module, url_prefix='/api/admin')

PUBLIC_ENDPOINTS = [
    'auth.info', 'auth.login', 'static', 'api_home', 'about', 'account.recover', 'account.recover_token'
]


@app.before_request
def before_request():
    if current_user.is_anonymous and request.endpoint not in PUBLIC_ENDPOINTS:
        abort(401)


@app.route('/api/home')
def api_home():
    page = StaticContent('home')
    title = page.title or "LadyBug Home"
    return dict(ok=True, title=title, html=page.html)


@app.route('/api/about')
def about():
    page = StaticContent('about')
    title = page.title or "About LadyBug"
    return dict(ok=True, title=title, html=page.html)


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    return dict(ok=False, msg=f"{e.code} {e.name}"), e.code


#######################################################################################################################

if app.debug:
    from flask import send_from_directory
    from os.path import join

    def debug_asset(path):
        """
        Assets pour le serveur debug
        """
        return send_from_directory(join(app.var_dir, 'assets'), path)

    def debug_theme():
        """
        Thème .css pour le serveur debug
        """
        theme = app.state['theme']
        from_dir = join(dirname(app.root_path), 'build', 'themes', theme)
        response = send_from_directory(from_dir, theme + '.css')
        response.cache_control.max_age = 0
        return response

    def debug_theme_dep(path):
        """
        Dépendance du thème pour le serveur debug
        """
        theme = app.state['theme']
        from_dir = join(dirname(app.root_path), 'build', 'themes', theme)
        response = send_from_directory(from_dir, path)
        response.cache_control.max_age = 0
        return response

    def debug_highlight():
        """
        Highlight .css pour le serveur debug
        """
        from_dir = join(dirname(app.root_path), 'build', 'highlight')
        response = send_from_directory(from_dir, app.state['highlight'] + '.css')
        response.cache_control.max_age = 0
        return response

    app.add_url_rule('/asset/<path:path>', 'debug_asset', debug_asset)
    app.add_url_rule('/debug_static/themes/preview.css', 'debug_theme', debug_theme)
    app.add_url_rule('/debug_static/themes/<path:path>', 'debug_theme_dep', debug_theme_dep)
    app.add_url_rule('/debug_static/highlight/preview.css', 'debug_highlight', debug_highlight)
    PUBLIC_ENDPOINTS.extend(('debug_theme', 'debug_highlight'))

#######################################################################################################################


@app.shell_context_processor
def shell_context_processor():
    from .database import db
    ns = dict(db=db)
    mods = list()
    parent = __name__.split('.')[0]
    for mod_name in ('account', 'kb'):
        mod = __import__(f'{parent}.{mod_name}.models', fromlist=(None,))
        for name in mod.__all__:
            ns[name] = getattr(mod, name)
            mods.append(name)
    print(' '.join(('Models :', *mods)))
    return ns
