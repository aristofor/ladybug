"""
État de l'app
Configuration dynamique, stockée dans var/data/appstate.yml
Rendu de var/public/index.html
"""

from pathlib import Path
from yaml import CDumper as Dumper, dump, CSafeLoader as Loader, load
from flask import render_template

DEFAULT_STATE = dict(
    sitename="LadyBug",
    description="A LadyBug site.",
    theme="Bulma",
    highlight="tango",
    page_rows=6,
    page_cols=3,
    tags_rows=12,
    tags_cols=4,
)

COERCE_STATE = dict(
    page_rows=int,
    page_cols=int,
    tags_rows=int,
    tags_cols=int,
)


class AppState():

    def __init__(self, app):
        self.app = app
        self.filename = Path(self.app.var_dir) / 'data' / 'appstate.yml'
        self._data = DEFAULT_STATE
        if self.filename.is_file():
            data = load(self.filename.read_text(), Loader=Loader)
            self.update(data)
        setattr(self.app, 'state', self)

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        if COERCE_STATE.get(key, None):
            value = COERCE_STATE[key](value)
        self._data[key] = value

    def items(self):
        return self._data.items()

    def update(self, data):
        for k, v in data.items():
            self.__setitem__(k, v)

    def save(self):
        with self.filename.open('w') as fd:
            dump(self._data,
                 stream=fd,
                 Dumper=Dumper,
                 allow_unicode=True,
                 explicit_start=True,
                 default_flow_style=False)

    def render_index_html(self):
        dest = Path(self.app.var_dir) / 'htdocs' / 'index.html'
        html = render_template('index.html', **self._data)
        dest.write_text(html)
