from subprocess import run, PIPE, CalledProcessError
from flask import current_app
import hashlib
from bs4 import BeautifulSoup
from urllib.parse import urlparse


def convert_markdown(source, res_id, enable_filter=True):
    pandoc_bin = current_app.config.get('PANDOC_BIN')
    pandoc_args = current_app.config.get('PANDOC_ARGS', [])
    pandoc_args += ['--from=markdown', '--to=html5']
    try:
        completed = run([pandoc_bin, *pandoc_args], input=source.encode('utf-8'), stdout=PIPE)
    except CalledProcessError:
        result = "Error, pandoc returncode {}".format(completed.returncode)
    else:
        result = completed.stdout.decode('utf-8')
    if enable_filter:
        result = html_filter(result, res_id.hex)
    return result


def render_markdown(text, res_id):
    res = None
    if text is None:
        text = ""
    enc = text.encode('utf-8')
    cache_key = 'render:markdown:' + hashlib.md5(enc).hexdigest()
    res = current_app.cache.get(cache_key)
    if res is None:
        res = convert_markdown(text, res_id)
        current_app.cache.add(cache_key, res, timeout=3600 * 24 * 10)
    return res


#######################################################################################################################


def html_filter(html, res_id_hex=None):
    """
    Filtre post-pandoc
    Ajoute class="title" aux headings
    Ajoute target="_blank" aux liens externes
    Lignes numérotées: change l'attribut 'title' en 'number'
    Images: fixe le chemin des assets en fonction de res_id_hex
    """
    soup = BeautifulSoup(html, 'lxml')
    for node in soup.find_all(('h1', 'h2', 'h3', 'h4', 'h5', 'h6')):
        node['class'] = 'title'
    for node in soup.find_all('a', href=True):
        parsed = urlparse(node['href'])
        if parsed.netloc != '':
            node['target'] = '_blank'
    for pre in soup.find_all('pre', class_='sourceCode'):
        num = 'numberSource' in pre['class']
        for node in pre.find_all('a', class_='sourceLine'):
            if num:
                node['number'] = node['title']
            del node['title']
    if res_id_hex:
        for node in soup.find_all('img'):
            parsed = urlparse(node['src'])
            if parsed.netloc == '' and parsed.path:
                node['src'] = '/'.join(('', 'asset', res_id_hex[-1], res_id_hex[-2], res_id_hex, node['src']))
    else:
        current_app.logger.warn("Missing res_id")
    return str(soup)
