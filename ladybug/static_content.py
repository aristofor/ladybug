from flask import current_app
from os import stat
from os.path import exists, join
from .renderer import convert_markdown
from .utils import parse_yaml_body


class StaticContent:
    """
    Contenu statique
    """

    _html = None

    def __init__(self, slug):
        self.slug = slug
        self.cache_key = "render:markdown:{}".format(slug)
        self._html = None
        self._title = None

    def _read(self):
        fname = join(current_app.var_dir, 'data', self.slug + '.md')
        if not exists(fname):
            self._html = ""
            self._title = ""
        else:
            cache = current_app.cache
            mtime = stat(fname).st_mtime
            data = cache.get(self.cache_key)
            if data is None or data['mtime'] < mtime:
                with open(fname, 'r') as fd:
                    meta, body = parse_yaml_body(fd.read())
                    self._html = convert_markdown(body, None, enable_filter=False)
                    self._title = meta.get('title', "")
                    cache.set(self.cache_key, dict(mtime=mtime, title=self._title, html=self._html))
            else:
                self._html = data['html']
                self._title = data['title']

    @property
    def html(self):
        if self._html is None:
            self._read()
        return self._html

    @property
    def title(self):
        if self._title is None:
            self._read()
        return self._title
