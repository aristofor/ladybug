all: build

.PHONY: build

INSTALL_DIR?=.
VAR_DIR?=${INSTALL_DIR}/var

BUILD_DIR=build.d
YARN_BUILD_DIR=build

build: themes highlight
	yarn build
	BUILD_DIR=${BUILD_DIR} tools/unrender
	cp -r ${BUILD_DIR}/templates ${VAR_DIR}
	cp -r ${BUILD_DIR}/themes ${YARN_BUILD_DIR}
	cp -r ${BUILD_DIR}/highlight ${YARN_BUILD_DIR}

clean:
	find ladybug -type d -name __pycache__ | xargs rm -rf

distclean: clean
	rm -rf ${BUILD_DIR} ${YARN_BUILD_DIR}

install_dirs:
	mkdir -p ${INSTALL_DIR}/venv ${INSTALL_DIR}/local
	for d in data run log dump assets trash htdocs templates; do mkdir -p ${VAR_DIR}/$$d; done

SASS=pysassc --output-style compressed

define theme_css
${BUILD_DIR}/themes/$(patsubst %.scss,%,$(notdir $(1)))/$(patsubst %.scss,%.css,$(notdir $(1)))
endef

define theme_rule
$(call theme_css, $(1)) : $(1)
	@mkdir -p $$(@D)
	$$(SASS) $$< > $$@
	@rsync -atv --exclude='*.scss' $$(<D) $$(@D)
endef

THEMES_SOURCE=$(wildcard src/themes/*/*.scss)
THEMES_CSS=$(foreach src, ${THEMES_SOURCE}, $(call theme_css, $(src)) )

$(foreach src, $(THEMES_SOURCE), $(eval $(call theme_rule, $(src))) )

themes: ${THEMES_CSS}

HIGHLIGHT_SOURCE=$(wildcard src/highlight/*.scss)
HIGHLIGHT_CSS=$(patsubst %.scss,${BUILD_DIR}/highlight/%.css, $(notdir ${HIGHLIGHT_SOURCE}))
${BUILD_DIR}/highlight/%.css: src/highlight/%.scss
	@mkdir -p $(@D)
	$(SASS) $< > $@

highlight: $(HIGHLIGHT_CSS)
