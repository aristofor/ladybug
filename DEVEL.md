Développement
=============

Paquets requis

~~~
apt-get install pandoc
~~~

Installation des dépendances js et python

~~~
yarn install --frozen-lockfile
make install_dirs
python3 -m venv --prompt LadyBug venv
. environ
pip install -r requirements.txt
make
~~~

Démarrer les serveurs debug :

~~~
. environ
flask run
~~~

dans un autre terminal :

~~~
yarn start
~~~

